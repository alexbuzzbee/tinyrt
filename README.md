TinyRT
======

TinyRT is a completely unnecessary portable runtime system. I don't recommend actually using it for anything; it's mostly a project to explore theory, and also learn Rust.

TinyRT essentially just implements a minimalistic (and probably poorly-designed) RISC-ish instruction set with a single logically-contiguous address space storing the code, the stack(s), and dynamic and static data. A module is simply a block of code and data loaded from a file. Modules can import other modules, which will be automatically located and loaded. A module's fully-qualified name is a URL, generally indicating where it can be found. A special instruction allows the running code to make requests of the runtime to do things like load modules and manage memory.