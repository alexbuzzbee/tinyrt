Module structure
================

A module is a file containing bytecode to be loaded into a running TinyRT instance. A module file consists of a linked list of records. Each record begins with the following 16-byte header (all numbers are big-endian):

| Field                | Size (bytes) | Offset (bytes) |
| -------------------- | ------------ | -------------- |
| Record size in bytes | 8            | 0              |
| Record type          | 1            | 8              |
| Reserved/padding     | 7            | 9              |

The record size includes the header. There are several different types of records, each containing different data:

* Module header (type `0x01`): The module's fully-qualified name, the minimum version of TinyRT, its type, and an entry point. Always the first record.
* Index (type `0x02`): A list of all records in the module to speed lookups. Always the second record.
* Exports table (type `0x03`): A list of symbols in the module's code to be exported to importing modules (imports are via a runtime API call).
* Code block (type `0x04`): An arbitrary-length blob of executable bytecode or read-only data to be placed in memory when the module is loaded.
* Data block (type `0x05`): An arbitrary-length blob of read-write data to be placed in memory when the module is loaded.

Module header
-------------

The module header takes the following structure:

| Field                            | Size (bytes) | Offset (bytes) |
| -------------------------------- | ------------ | -------------- |
| Minimum version                  | 2            | 0              |
| Module type                      | 1            | 2              |
| Entry point index                | 2            | 3              |
| Name length                      | 2            | 5              |
| Fully-qualified name (UTF-8 URL) | n            | 7              |

The minimum version is the required internal version of TinyRT. The internal version is incremented once for every release; it is not related to the major/minor/patch human-friendly version number.

The module type is one of the following:

* `0x00`: Invalid module.
* `0x01`: Program module. Can be executed.
* `0x02`: Library module. Can be loaded by other modules during execution.
* `0x03`: Incomplete module. Not loadable; requires further processing by other software.

The entry point is mainly applicable to programs. It is the index of an entry in the export table.

Index
-----

The index takes the following structure:

| Field             | Size (bytes) | Offset (bytes) |
| ----------------- | ------------ | -------------- |
| Number of entries | 2            | 0              |
| Index entries     | 9 * n        | 2              |

Each entry takes the following structure:

| Field       | Size (bytes) | Offset (bytes) |
| ----------- | ------------ | -------------- |
| Record type | 1            | 0              |
| Offset      | 8            | 1              |

The index is meant to speed lookups for large modules by avoiding scans of the record chain.

Exports table
-------------

The exports table takes the following structure:

| Field             | Size (bytes) | Offset (bytes) |
| ----------------- | ------------ | -------------- |
| Number of entries | 2            | 0              |
| Export entries    | 9 * n        | 2              |

Each entry takes the following structure:

| Field       | Size (bytes) | Offset (bytes) |
| ----------- | ------------ | -------------- |
| Type        | 1            | 0              |
| Offset      | 8            | 1              |
| Name length | 2            | 9              |
| Name        | n            | 11             |

The offset is measured within a concatenation of all the code and data blocks in the order they appear in the module, which is the same arrangement as when the module is loaded. The type is one of the following:

* `0x00`: Null/invalid export.
* `0x01`: Function.
* `0x02`: Read-only variable.
* `0x03`: Read-write variable.
