Code organization
=================

Most of the runtime consists of a library crate called `tinyrtlib`. This crate contains all necessary components for running TinyRT code. It consists of the following elements:

* The module parser. Also used to emit modules.
* The interpreter.
  * The execution module, responsible for interpreting and running individual bytecode instructions.
  * The register file, responsible for storing the data in the registers.
  * The caster, responsible for converting values between Rust `u64`s storable in registers and bytes stored in memory.
* The memory manager, responsible for presenting the logically contiguous memory address space and allocating and freeing memory.
* The runtime API.