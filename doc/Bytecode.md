Bytecode
========

The TinyRT bytecode instruction set is a relatively simple RISC-ish ISA with limited capabilities. Each instruction is a 1-byte opcode followed by a variable number of bytes of parameters.

The virtual machine has 64 64-bit registers numbered r0 to r63. r63 is the stack pointer and r62 is the standard link register. A register may be specified with a 6-bit index, usually stored in a whole byte. It also has a 64-bit byte-addressable address space. Loads and stores are big-endian.

* `00`/`NOP`: No-op.

---

* `01`/`LOADw rX aY`: Load register X with 8 bytes from address Y.
* `02`/`LOADh rX aY`: Load the low bytes of register X with 4 bytes from address Y, zeroing the higher bytes.
* `03`/`LOADq rX aY`: Load the low bytes of register X with 2 bytes from address Y, zeroing the higher bytes.
* `04`/`LOADB rX aY`: Load the low byte of register X with a byte from address Y, zeroing the higher bytes.

---

* `05`/`STORw rX aY`: Store 8 bytes from register X to address Y.
* `06`/`STORh rX aY`: Store the 4 low bytes of register X to address Y.
* `07`/`STORq rX aY`: Store the 2 low bytes of register X to address Y.
* `08`/`STORB rX aY`: Store the low byte of register X to address Y.

---

* `09`/`JUMPu aX`: Unconditionally jump to address X.
* `0A`/`JUMPg aX`: Jump if greater to address X.
* `0B`/`JUMPl aX`: Jump if lesser to address X.
* `0C`/`JUMPe aX`: Jump if equal to address X.
* `0D`/`JUMPr rX`: Unconditionally jump to the address stored in register X.

---

* `0E`/`CALL aX rY`: Store the address of the next instruction in register Y and jumps to address X.
* `0F`/`CALLr rX rY`: Store the address of the next instruction in register Y and jumps to the address stored in register X.

---

* `10`/`COMP rX rY`: Compare the contents of registers X and Y and store the result for a conditional jump.

---

* `11`/`ADD rX rY`: Add the contents of register Y to register X and store the result in register X.
* `12`/`SUB rX rY`: Subtract the contents of register Y from register X and store the result in register X.
* `13`/`MUL rX rY`: Multiply the contents of register Y by register X and store the result in register X.
* `14`/`DIV rX rY`: Divide the contents of register Y by register X and store the result in register X.

---

* `15`/`ADDu rX rY`: Add the contents of register Y to register X as unsigned and store the result in register X.
* `16`/`SUBu rX rY`: Subtract the contents of register Y from register X as unsigned and store the result in register X.
* `17`/`MULu rX rY`: Multiply the contents of register Y by register X as unsigned and store the result in register X.
* `18`/`DIVu rX rY`: Divide the contents of register Y by register X as unsigned and store the result in register X.

---

* `19`/`ADDf rX rY`: Add the contents of register Y to register X as IEEE 754 double-precision and store the result in register X.
* `1A`/`SUBf rX rY`: Subtract the contents of register Y from register X as IEEE 754 double-precision and store the result in register X.
* `1B`/`MULf rX rY`: Multiply the contents of register Y by register X as IEEE 754 double-precision and store the result in register X.
* `1C`/`DIVf rX rY`: Divide the contents of register Y by register X as IEEE 754 double-precision and store the result in register X.

---

* `1D`/`SHL rX rY`: Shift register X's contents left by a number of bits equal to the contents of register Y.
* `1E`/`SHR rX rY`: Shift register X's contents right by a number of bits equal to the contents of register Y.
* `1F`/`ROTL rX rY`: Rotate register X's contents left by a number of bits equal to the contents of register Y.
* `20`/`ROTR rX rY`: Rotate register X's contents right by a number of bits equal to the contents of register Y.

---

* `21`/`AND rX rY`: Perform a bitwise AND between register X and register Y and stores the result in register X.
* `22`/`OR rX rY`: Perform a bitwise OR between register X and register Y and stores the result in register X.
* `23`/`XOR rX rY`: Perform a bitwise XOR between register X and register Y and stores the result in register X.

---

* `25`/`PUSHw rX`: Push the 8 bytes in register X onto the stack.
* `26`/`PUSHh rX`: Push the low 4 bytes in register X onto the stack.
* `27`/`PUSHq rX`: Push the low 2 bytes in register X onto the stack.
* `28`/`PUSHb rX`: Push the low byte in register X onto the stack.

---

* `29`/`POPw rX`: Pop 8 bytes off of the stack and into register X.
* `2A`/`POPh rX`: Pop 4 bytes off of the stack and into register X.
* `2B`/`POPq rX`: Pop 2 bytes off of the stack and into register X.
* `2C`/`POPb rX`: Pop 1 byte off of the stack and into register X.

---

* `2D`/`RTCL X`: Perform the runtime API call number X. The runtime API call number is 32 bits. Runtime API calls use the registers for parameters and return value.