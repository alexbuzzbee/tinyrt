Runtime API calls
=================

The runtime API uses the registers to receive its arguments and provide its return value. It always uses the lowest registers first, and never uses the high 32 registers. Large objects are passed by address, and the runtime API may allocate memory in which to place its return value.

<!-- TODO: Write actual API functions. -->