pub enum ExecutionError {
  BadOpcode,
  BadRegister,
  ExecutionEnded,
  MemoryError(::mem::MemoryError),
}

pub struct ExecutionUnit<'a> {
  regs: super::registers::RegisterFile,
  memory: ::mem::AddressSpace<'a>,
  nextInstruction: u64,
}

impl<'a> ExecutionUnit<'a> {
  pub fn new(mem: ::mem::AddressSpace) -> ExecutionUnit {
    ExecutionUnit {
      regs: super::registers::RegisterFile([0; 64]),
      memory: mem,
      nextInstruction: 0, // Entry point set when we start running code.
    }
  }

  pub fn executeInstruction(&self, instruction: &[u8], address: u64) -> Result<u64, ExecutionError> { // Returns number of bytes consumed. Jumps return 0, because they modify nextInstruction.
    if !self.memory.checkPermission(address, 1, ::mem::MemoryOperation::Execute) {
      return Err(ExecutionError::MemoryError(::mem::MemoryError::UnpermittedOperation))
    }
    let opcode = instruction[0];
    match opcode {
      0x00 => Ok(1),
      _ => Err(ExecutionError::BadOpcode)
    }
  }

  pub fn executeStream(&mut self, address: u64) -> Option<ExecutionError> {
    self.nextInstruction = address;
    loop {
      match self.executeInstruction(&self.memory.getBytes(self.nextInstruction, 1), self.nextInstruction) {
        Ok(size) => {
          self.nextInstruction += size
        },
        Err(ExecutionError::ExecutionEnded) => return None,
        Err(e) => return Some(e)
      };
    }
  }
}