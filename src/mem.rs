use std::iter::FromIterator;

pub enum MemoryError {
  NonMappedAddress,
  UnpermittedOperation,
}

pub enum MemoryOperation {
  Read,
  Write,
  Execute,
  Unmap,
}

pub struct MemoryBlock {
  data: Vec<u8>,
  allowRead: bool,
  allowWrite: bool,
  allowExec: bool,
  allowUnmap: bool,
}

impl MemoryBlock {
  pub fn checkPermission(&self, op: MemoryOperation) -> bool {
    match op {
      Read => {self.allowRead},
      Write => {self.allowWrite},
      Execute => {self.allowExec},
      Unmap => {self.allowUnmap},
    }
  }
}

pub struct AddressSpace<'a> {
  blocks: &'a Vec<MemoryBlock>,
}

impl<'a> AddressSpace<'a> { // TODO: Either handle operations that cross a block boundry or detect them and make them illegal.
  pub fn findAddress(&self, address: u64) -> Result<(usize, u64), MemoryError> { // If successful, returns (block number, offset).
    let mut total = 0 as u64;
    let mut blockNum = 0;
    for block in self.blocks {
      blockNum += 1;
      if address < total + block.data.len() as u64 {
        Ok((blockNum, address - total))
      }
      total += block.data.length;
    }
    Err(MemoryError::NonMappedAddress)
  }

  pub fn checkPermission(&self, address: u64, size: usize, op: MemoryOperation) -> Result<bool, MemoryError> {
    Ok(self.blocks[self.findAddress(address)?.0].checkPermission(op))
  }

  pub fn getBytes(&self, address: u64, size: usize) -> Vec<u8> {
    let address: usize = address as usize;
    Vec::from_iter(self.data[address..address+size].iter().cloned())
  }

  pub fn writeBytes(&mut self, address: u64, data: Vec<u8>) -> Option<MemoryError> {
    // TODO: Implement this.
    None
  }
}