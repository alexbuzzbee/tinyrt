#![allow(non_snake_case)]
#![cfg(target_pointer_width = "32")] // We cast u64 to usize, so until that's fixed it'll only work reliably on 64-bit or above.
compile_error!("This crate (TinyRTLib) cannot be compiled for 32-bit systems.");

pub mod module_parse;
pub mod interpret;
pub mod mem;

// Invokes a program module with the specified arguments.
pub fn invokeProgram(program: String, args: Vec<String>) -> i8 {
  // TODO: Write this.
  0
}