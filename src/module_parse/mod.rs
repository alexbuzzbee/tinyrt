extern crate byteorder; // Imported relative to this module.

use std::string;
use std::error;
use std::fmt;
use self::byteorder::{BigEndian, ByteOrder};

// Errors.

#[derive(Debug)]
pub enum ParseError {
  BadValue,
  BufferTooSmall(usize, usize),
}

impl error::Error for ParseError {
  fn cause(&self) -> Option<&error::Error> {
    match self {
      ParseError::BadValue => None,
      ParseError::BufferTooSmall(_, _) => None,
    }
  }
}

impl fmt::Display for ParseError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      ParseError::BadValue => {
        write!(f, "Invalid type number")
      },
      ParseError::BufferTooSmall(real, expected) => {
        write!(f, "Buffer too small; was {} bytes, expected {} bytes.", real, expected)
      }
    }
  }
}

// Basic (universal) record headers.

pub enum RecordType {
  ModuleHeader,
  Index,
  Exports,
  Code,
  Data,
}

pub struct RecordHeader {
  pub size: u64,
  pub recType: RecordType,
}

impl RecordHeader {
  pub fn parse(buf: &[u8]) -> Result<RecordHeader, ParseError> {
    if buf.len() < 9 {
      return Err(ParseError::BufferTooSmall(buf.len(), 9));
    }
    Ok(RecordHeader {
      size: BigEndian::read_u64(&buf[..8]),
      recType: match buf[8] {
        1 => RecordType::ModuleHeader,
        2 => RecordType::Index,
        3 => RecordType::Exports,
        4 => RecordType::Code,
        5 => RecordType::Data,
        _ => {
          return Err(ParseError::BadValue);
        },
      },
    })
  }
}

// The main module header.

pub struct ModuleHeader {
  pub name: String,
  pub rtVersion: u16,
  pub modType: u8,
  pub entry: u16,
}

impl ModuleHeader {
  pub fn parse(buf: &[u8]) -> Result<ModuleHeader, ParseError> {
    if buf.len() < 8 {
      return Err(ParseError::BufferTooSmall(buf.len(), 8));
    }
    let nameSize = BigEndian::read_u16(&buf[4..6]) as usize;
    Ok(ModuleHeader {
      rtVersion: BigEndian::read_u16(&buf[0..2]),
      modType: buf[2],
      entry: BigEndian::read_u16(&buf[2..4]),
      name: string::String::from_utf8_lossy(&buf[6..nameSize + 6]).into_owned(),
    })
  }
}

// The index.

pub struct IndexEntry {
  pub recType: u8,
  pub offset: u64,
}

impl IndexEntry {
  fn parse(buf: &[u8]) -> Result<IndexEntry, ParseError> {
    if buf.len() < 9 {
      return Err(ParseError::BufferTooSmall(buf.len(), 9));
    }
    Ok(IndexEntry {
      recType: buf[0],
      offset: BigEndian::read_u64(&buf[1..9]),
    })
  }
}

pub struct Index(Vec<IndexEntry>);

impl Index {
  pub fn parse(buf: &[u8]) -> Result<Index, ParseError> {
    if buf.len() < 2 {
      return Err(ParseError::BufferTooSmall(buf.len(), 2));
    }
    let entries = BigEndian::read_u16(&buf[0..2]) as usize;
    let mut result = Vec::<IndexEntry>::new();
    for i in 0..entries {
      let offset = 2 + (i * 9);
      result.insert(i, match IndexEntry::parse(&buf[offset..offset + 1]) {
        Ok(ent) => ent,
        Err(err) => {
          return Err(err);
        }
      });
    }
    Ok(Index(result))
  }
}

// The export table.

#[derive(Debug)]
pub enum ExportType {
  Null,
  Function,
  ReadVar,
  ReadWriteVar,
}

pub struct Export {
  pub expType: ExportType,
  pub offset: u64,
  pub name: String,
}

impl Export {
  pub fn parse(buf: &[u8]) -> Result<Export, ParseError> {
    if buf.len() < 12 {
      return Err(ParseError::BufferTooSmall(buf.len(), 12));
    }
    let nameSize = BigEndian::read_u16(&buf[9..11]) as usize;
    Ok(Export {
      expType: match buf[0] {
        0 => ExportType::Null,
        1 => ExportType::Function,
        2 => ExportType::ReadVar,
        3 => ExportType::ReadWriteVar,
        _ => {
          return Err(ParseError::BadValue);
        }
      },
      offset: BigEndian::read_u64(&buf[1..9]),
      name: string::String::from_utf8_lossy(&buf[11..nameSize]).into_owned(),
    })
  }
}

pub struct Exports(Vec<Export>);

impl Exports {
  pub fn parse(buf: &[u8]) -> Result<Exports, ParseError> {
    if buf.len() < 2 {
      return Err(ParseError::BufferTooSmall(buf.len(), 2));
    }
    let entries = BigEndian::read_u16(&buf[0..2]) as usize;
    let mut result = Vec::<Export>::new();
    for i in 0..entries {
      let offset = 2 + (i * 9);
      result.insert(i, match Export::parse(&buf[offset..offset + 1]) {
        Ok(exp) => exp,
        Err(err) => {
          return Err(err);
        }
      });
    }
    Ok(Exports(result))
  }
}

pub enum Record<'a> {
  ModuleHeader(ModuleHeader),
  Index(Index),
  Exports(Exports),
  Code(&'a [u8]),
  Data(&'a [u8]),
}

impl<'a> Record<'a> {
  pub fn parse(buf: &[u8]) -> Result<Record, ParseError> {
    if buf.len() < 16 {
      return Err(ParseError::BufferTooSmall(buf.len(), 16));
    }
    
    let recHeader = match RecordHeader::parse(&buf[0..16]) {
      Ok(header) => header,
      Err(err) => {
        return Err(err);
      },
    };

    let size = recHeader.size as usize;
    
    if buf.len() < recHeader.size as usize {
      return Err(ParseError::BufferTooSmall(buf.len(), size));
    }

    return Ok(match recHeader.recType {
      RecordType::ModuleHeader => {
        Record::ModuleHeader(match ModuleHeader::parse(&buf[16..size]) {
          Ok(rec) => rec,
          Err(err) => return Err(err),
        })
      },
      RecordType::Index => {
        Record::Index(match Index::parse(&buf[16..size]) {
          Ok(rec) => rec,
          Err(err) => return Err(err),
        })
      },
      RecordType::Exports => {
        Record::Exports(match Exports::parse(&buf[16..size]) {
          Ok(rec) => rec,
          Err(err) => return Err(err),
        })
      },
      RecordType::Code => Record::Code(&buf[16..size]),
      RecordType::Data => Record::Data(&buf[16..size]),
    });
  }
}

#[cfg(test)]
mod test {
  use std::fs::File;
  use std::io;
  use std::io::prelude::*;
  use super::*;

  #[test]
  fn parseModuleHeader() {
    let mut f = File::open("data/test_modheader.trmod").expect("Opening test_modheader.trmod");
    let mut buf = [0; 41];

    f.read(&mut buf[..]).expect("Reading test_modheader.trmod");

    match Record::parse(&buf[..]).expect("Parsing test module header") {
      Record::ModuleHeader(header) => {
        assert_eq!(header.name, "file://./test.trmod");
        assert_eq!(header.rtVersion, 1);
        assert_eq!(header.modType, 0);
        assert_eq!(header.entry, 0);
      },
      _ => panic!("Test module header didn't parse as a module header."),
    };
  }

  #[test]
  #[should_panic(expected = "Parsing test module header: BufferTooSmall(20, 41)")]
  fn errorOnTruncatedRecord() {
    let mut f = File::open("data/test_modheader.trmod").expect("Opening test_modheader.trmod");
    let mut buf = [0; 20];

    f.read(&mut buf[..]).expect("Reading test_modheader.trmod");

    match Record::parse(&buf[..]).expect("Parsing test module header") {
      Record::ModuleHeader(header) => {
        assert_eq!(header.name, "file://./test.trmod");
        assert_eq!(header.rtVersion, 1);
        assert_eq!(header.modType, 0);
        assert_eq!(header.entry, 0);
      },
      _ => panic!("Test module header didn't parse as a module header."),
    };
  }

  #[test]
  fn parseIndex() {
    let mut f = File::open("data/test_index.trmod").expect("Opening test_index.trmod");
    let mut buf = [0; 35];

    f.read(&mut buf[..]).expect("Reading test_index.trmod");

    match Record::parse(&buf[..]).expect("Parsing test index") {
      Record::Index(index) => {
        assert_eq!(index.0[0].recType, 1);
        assert_eq!(index.0[0].offset, 0);
        assert_eq!(index.0[1].recType, 2);
        assert_eq!(index.0[1].offset, 41);
      },
      _ => panic!("Test index didn't parse as an index."),
    };
  }

  #[test]
  fn parseExports() {
    let mut f = File::open("data/test_exports.trmod").expect("Opening test_exports.trmod");
    let mut buf = [0; 41]; // TODO: Fix size once we have an example.

    f.read(&mut buf[..]).expect("Reading test_exports.trmod");

    match Record::parse(&buf[..]).expect("Parsing test exports") {
      Record::Exports(exports) => {
        //assert_eq!(exports.0[0].expType, ExportType::Function); // TODO: Fix (implement ==).
        assert_eq!(exports.0[0].offset, 0);
        assert_eq!(exports.0[0].name, "main");
        //assert_eq!(exports.0[1].expType, ExportType::ReadVar); // TODO: Fix (implement ==).
        assert_eq!(exports.0[1].offset, 4096);
        assert_eq!(exports.0[1].name, "strings");
      },
      _ => panic!("Test exports didn't parse as an exports table."),
    };
  }
}
