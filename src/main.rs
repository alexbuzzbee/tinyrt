extern crate tinyrtlib;

use std::env;

fn main() {
  let args: Vec<String> = env::args().collect();
  tinyrtlib::invokeProgram(args[0].clone(), args[1..].to_vec());
}